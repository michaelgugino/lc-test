terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.63.0"
    }
  }
}

provider "aws" {
  # Configuration options
}

variable "aws-account-id" {
  type        = string
  description = "ID of your aws account"
}

# from https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role
resource "aws_iam_role" "prod-ci-role" {
  name = "prod-ci-role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          "AWS": "arn:aws:iam::${var.aws-account-id}:root"
        }
      },
    ]
  })

  tags = {
    tag-key = "prod-ci"
  }
}

resource "aws_iam_group" "prod-ci-group" {
  name = "prod-ci-group"
  path = "/"
}

resource "aws_iam_user_group_membership" "prod-ci-group" {
  user = aws_iam_user.prod-ci-user.name

  groups = [
    aws_iam_group.prod-ci-group.name,
  ]
}

resource "aws_iam_user" "prod-ci-user" {
  name = "prod-ci-user"
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy
# https://stackoverflow.com/questions/34922920/how-can-i-allow-a-group-to-assume-a-role

resource "aws_iam_policy" "policy" {
  name        = "prod-ci-policy"
  description = "A policy"
  path = "/"
  policy      = jsonencode({
      "Version": "2012-10-17",
      "Statement": [
          {
              "Effect": "Allow",
              "Action": [
                  "sts:AssumeRole"
              ],
              "Resource": [
                  "arn:aws:iam::${var.aws-account-id}:role/prod-ci-role"
              ]
          }
      ]
  })
}

resource "aws_iam_group_policy_attachment" "prod-ci-attach" {
  group      = aws_iam_group.prod-ci-group.name
  policy_arn = aws_iam_policy.policy.arn
}
