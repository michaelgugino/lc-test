#!/usr/bin/env python

import re
import sys

# precompile regex, much faster
pattern = re.compile('(\d{4}-\d{1,2}-\d{1,2})T')

for line in sys.stdin:
    m = pattern.match(line)
    if m:
        # retrieve match from first parenthesis
        ms = m.group(1)
        ml = ms.split('-')
        nm = '-'.join([ml[1], ml[2], ml[0]])
        s = m.span(1)
        # last char is \n, don't want that here.
        print(nm + line[s[1]:-1])
    else:
        print(line[0:-1])
