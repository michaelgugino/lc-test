FROM docker.io/fedora:latest as downloader
WORKDIR /app
RUN dnf install -y gpg wget
# Ensure we're using TLS
# This step can be really flaky, in the future might want to obtain from a
# local mirror or elsewhere.
# RUN gpg --keyserver hkps://pgp.mit.edu:443 --recv-key FE3348877809386C
RUN gpg --keyserver pgp.mit.edu --recv-key FE3348877809386C
# TODO: Ideally, we verify this key via chain of trust.
COPY tools/download-helper.sh download-helper.sh
# Download and verify the release in a single step so we don't cache a bad dl.
RUN ./download-helper.sh

# litecoin binaries are not statically linked, so we need an image that provides
# libc, etc.
FROM docker.io/fedora:latest
WORKDIR /app
COPY --from=downloader /app/litecoin-0.18.1/bin/* /app/
RUN adduser lcuser
RUN chown -R lcuser:lcuser /app
USER lcuser

ENTRYPOINT ["/app/litecoind"]
