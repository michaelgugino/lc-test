#!/bin/bash

# Exit on any error
set -e

# Instructions said to verify checksum, but there is not a published checksum.
verify_target () {
# Steps to verify file based on: https://download.litecoin.org/README-HOWTO-GPG-VERIFY-TEAM-MEMBERS-KEY.txt
gpg --verify $2 $1
}

arch=$(uname -i)

# TODO: use case expression in the future, mutate $arch to match substring in
# corresponding filenames
if [ "$arch" = "x86_64" ]; then
    f1="litecoin-0.18.1-x86_64-linux-gnu.tar.gz"
    f2="litecoin-0.18.1-x86_64-linux-gnu.tar.gz.asc"
    wget -q https://download.litecoin.org/litecoin-0.18.1/linux/$f1
    wget -q https://download.litecoin.org/litecoin-0.18.1/linux/$f2
    verify_target $f1 $f2
    echo "verified download"
    tar xf $f1
    exit 0
fi

# TODO: Add other supported arches here.

echo "unknown arch"
exit 1
